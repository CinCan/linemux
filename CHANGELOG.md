# Change log

## [0.1.1]

- Added: Limit file name length to 128, sconfigurable by --max-name-length or -L

## [0.1.0]

- Added: Initial beta version

